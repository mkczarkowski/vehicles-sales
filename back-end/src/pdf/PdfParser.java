package pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdfParser {
	private static String FILE_NAME = "vehicles.pdf";

	public static void writeText(String s) {

		Document document = new Document();

		try {

			PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));

			//open
			document.open();

			Paragraph p = new Paragraph();
			//p.add("vehicles");
		//	p.setAlignment(Element.ALIGN_CENTER);

			p.add(s);
			document.add(p);

			//close
			document.close();		

		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	

}
