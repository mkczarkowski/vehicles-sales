package httpserver.parser;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryParser {

	public static Map parse(String query) throws UnsupportedEncodingException {

		Map<String, String> parameters = new LinkedHashMap<>();
		
		if (query != null) {
			String pairs[] = query.split("[&]");
			for (String pair : pairs) {
				String param[] = pair.split("[=]");
				String key = null;
				String value = null;
				if (param.length > 0) {
					key = URLDecoder.decode(param[0], System.getProperty("file.encoding"));
				}

				if (param.length > 1) {
					value = URLDecoder.decode(param[1], System.getProperty("file.encoding"));

					parameters.put(key, value);
				}
			}
		}
		return parameters;
	}
}
