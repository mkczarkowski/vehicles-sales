package httpserver;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StartServer extends Application{


	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/windowView.fxml"));
		primaryStage.setTitle("Http Server");
		primaryStage.setScene(new Scene(root, 303, 130));
		primaryStage.setResizable(false);
		primaryStage.show();
	}
}
