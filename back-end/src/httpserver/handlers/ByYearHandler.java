package httpserver.handlers;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import vehicles.map.MapSalesByCategory;
import vehicles.map.builder.Director;
import vehicles.map.builder.MapSalesByYear;
import httpserver.parser.QueryParser;

public class ByYearHandler implements HttpHandler {

    public void handle(HttpExchange he) throws IOException {
        Map<String, String> parameters;
        URI requestedUri = he.getRequestURI();
        String query = requestedUri.getRawQuery();
        parameters = QueryParser.parse(query);

        String response = "";
        for (String year : parameters.keySet()) {
            Director director = new Director();
            MapSalesByYear builder = new MapSalesByYear(parameters.get(year));
            director.setBuilder(builder);
            director.build();
            MapSalesByCategory vehiclesGetParticularData = director.getVehicleGetParticularData();
            response = vehiclesGetParticularData.getDataByYear();
        }
        he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
        he.sendResponseHeaders(200, response.length());
        OutputStream os = he.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
