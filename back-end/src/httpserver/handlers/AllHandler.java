package httpserver.handlers;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import vehicles.map.MapSalesByCategory;

public class AllHandler implements HttpHandler {

	public void handle(HttpExchange he) throws IOException {
		MapSalesByCategory getVehiclesData = MapSalesByCategory.createObject();
		try {
			String response = getVehiclesData.getAllSalesData();
			he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.getBytes());
			os.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
