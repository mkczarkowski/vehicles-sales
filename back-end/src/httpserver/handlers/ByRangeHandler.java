package httpserver.handlers;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import httpserver.parser.QueryParser;
import vehicles.map.MapSalesByCategory;
import vehicles.map.builder.Director;
import vehicles.map.builder.MapSalesBySalesRange;

public class ByRangeHandler implements HttpHandler {

	public void handle(HttpExchange he) throws IOException {
		

		URI requestedUri = he.getRequestURI();
		String rawQuery = requestedUri.getRawQuery();
		Map<String, String> parsedQuery = QueryParser.parse(rawQuery);
		List<String> queryValues = new ArrayList<String>();
		for (String key : parsedQuery.keySet()) {
			queryValues.add(parsedQuery.get(key));
		}
		Director director = new Director();
		MapSalesBySalesRange builder = new MapSalesBySalesRange(queryValues.get(0), queryValues.get(1));
		director.setBuilder(builder);
		director.build();

		MapSalesByCategory vehiclesGetParticularData = director.getVehicleGetParticularData();
		String response = vehiclesGetParticularData.getDataByVehiclesSoldRange();
		he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
		he.sendResponseHeaders(200, response.length());
		OutputStream os = he.getResponseBody();
		os.write(response.getBytes());
		os.close();

	}
}
