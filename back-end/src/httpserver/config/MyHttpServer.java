package httpserver.config;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import httpserver.handlers.AllHandler;
import httpserver.handlers.ByRangeHandler;
import httpserver.handlers.ByRegOrCountryHandler;
import httpserver.handlers.ByYearHandler;

public class MyHttpServer {

	private int port;
	private HttpServer server;
	
	public void start(int port) throws IOException {
		this.port = port;
				
		server = HttpServer.create(new InetSocketAddress(port), 0);
		//System.out.println("server started at " + port);
		server.createContext("/vehicles-sales-getAll", new AllHandler());
		
		//z parametrami
		//np.: /vehicles-sales-getByRange?start=100&stop=2000
		server.createContext("/vehicles-sales-getByRange", new ByRangeHandler());
		//np.: /vehicles-sales-getByRegionOrCountry?region=russia
		server.createContext("/vehicles-sales-getByRegionOrCountry", new ByRegOrCountryHandler());
		//np.: /vehicles-sales-getByYear?year=2006
		server.createContext("/vehicles-sales-getByYear", new ByYearHandler());
		server.setExecutor(null);
		server.start();
	}
	
}
