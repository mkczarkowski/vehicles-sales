package httpserver;

import httpserver.config.MyHttpServer;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.io.IOException;

public class StartServerController {

    public Label label;
    private static int port = 9090;

    public StartServerController() {
    }

    public void handleStartServerButton() {
        MyHttpServer myHttpServer = new MyHttpServer();
        try {
            myHttpServer.start(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        label.setVisible(true);

        }
    @FXML
    public void initialize(){
        label.setVisible(false);
    }
}