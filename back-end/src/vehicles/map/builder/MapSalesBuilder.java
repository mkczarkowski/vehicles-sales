package vehicles.map.builder;

import vehicles.map.MapSalesByCategory;

public abstract class MapSalesBuilder {

	protected MapSalesByCategory vehiclesGetParticularData;

	public void newVehicleObject() {
		vehiclesGetParticularData = MapSalesByCategory.createObject();
	}

	public MapSalesByCategory getVehicleObject() {
		return vehiclesGetParticularData;
	}

	public abstract void buildByCountryOrRegion();

	public abstract void buildByYear();

	public abstract void buildBySalesRange();

}

