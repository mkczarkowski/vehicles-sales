package vehicles.map.builder;

import vehicles.map.MapSalesByCategory;

public 	class Director {

	private MapSalesBuilder mapSalesBuilder;

	public void setBuilder(MapSalesBuilder mapSalesBuilder) {
		this.mapSalesBuilder = mapSalesBuilder;
	}

	public MapSalesByCategory getVehicleGetParticularData() {

		return mapSalesBuilder.getVehicleObject();
	}
	
	public void build() {
		mapSalesBuilder.newVehicleObject();
		mapSalesBuilder.buildByCountryOrRegion();
		mapSalesBuilder.buildByYear();
		mapSalesBuilder.buildBySalesRange();
	}

}
