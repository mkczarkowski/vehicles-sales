package vehicles.map.builder;

public class MapSalesByYear extends MapSalesBuilder {

	String countryOrRegion;
	String year;
	String startRange;
	String endRange;

	public MapSalesByYear(String year) {
		this.year = year;
	}

	@Override
	public void buildByCountryOrRegion() {
		vehiclesGetParticularData.setCountryOrRegion(null);
	}

	@Override
	public void buildByYear() {
		vehiclesGetParticularData.setYear(year);

	}

	@Override
	public void buildBySalesRange() {
		vehiclesGetParticularData.setEndRange(null);
		vehiclesGetParticularData.setStartRange(null);

	}

}
