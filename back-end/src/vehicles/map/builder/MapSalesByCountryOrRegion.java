package vehicles.map.builder;

public class MapSalesByCountryOrRegion extends MapSalesBuilder {

	String countryOrRegion;
	String year;
	String startRange;
	String endRange;

	public MapSalesByCountryOrRegion(String countryOrRegion) {
		this.countryOrRegion = countryOrRegion;
	}

	@Override
	public void buildByCountryOrRegion() {
		vehiclesGetParticularData.setCountryOrRegion(countryOrRegion);
	}

	@Override
	public void buildByYear() {
		vehiclesGetParticularData.setYear(null);

	}

	@Override
	public void buildBySalesRange() {
		vehiclesGetParticularData.setEndRange(null);
		vehiclesGetParticularData.setStartRange(null);

	}

}
