package vehicles.map.builder;

public class MapSalesBySalesRange extends MapSalesBuilder {

	String countryOrRegion;
	String year;
	String startRange;
	String endRange;

	public MapSalesBySalesRange(String startRange, String endRange) {
		this.startRange = startRange;
		this.endRange = endRange;
	}

	@Override
	public void buildByCountryOrRegion() {
		vehiclesGetParticularData.setCountryOrRegion(null);
	}

	@Override
	public void buildByYear() {
		vehiclesGetParticularData.setYear(null);

	}

	@Override
	public void buildBySalesRange() {
		vehiclesGetParticularData.setEndRange(endRange);
		vehiclesGetParticularData.setStartRange(startRange);

	}
}