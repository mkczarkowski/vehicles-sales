package vehicles.map;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import pdf.PdfParser;
import vehicles.db.DBConnector;
import vehicles.parser.JsonParser;

public class MapSalesByCategory {

	private Workbook workbook;
	private DataFormatter dataFormatter;
	private int rowIndex;
	private long vehiclesSaled;
	private String CountryOrRegion;
	private String year;
	private String startRange;
	private String endRange;

	public DataFormatter getDataFormatter() {
		return dataFormatter;
	}

	public void setDataFormatter(DataFormatter dataFormatter) {
		this.dataFormatter = dataFormatter;
	}

	public String getCountryOrRegion() {
		return CountryOrRegion;
	}

	public void setCountryOrRegion(String countryOrRegion) {
		CountryOrRegion = countryOrRegion;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getStartRange() {
		return startRange;
	}

	public void setStartRange(String startRange) {
		this.startRange = startRange;
	}

	public String getEndRange() {
		return endRange;
	}

	public void setEndRange(String endRange) {
		this.endRange = endRange;
	}



	public MapSalesByCategory() {
		dataFormatter = new DataFormatter();
		rowIndex = 7;
	}

	public static MapSalesByCategory createObject() {
		return new MapSalesByCategory();
	}

	public String getDataByCountryOrRegion() throws IOException {
		Sheet sheet = getDatabase();
		Map<Integer, Long> dataByCountryOrRegion = new LinkedHashMap<>();
		int saleYear = 2005;

		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				if (cellValue.equalsIgnoreCase(this.getCountryOrRegion())) {
					for (Cell c : row) {
						if (c.getCellTypeEnum().name().equals("NUMERIC")) {
							vehiclesSaled = Long.parseLong(dataFormatter.formatCellValue(c).replaceAll("\\D", ""));
							dataByCountryOrRegion.put(saleYear, vehiclesSaled);
							saleYear++;
						}
					}
				}
			}
		}
		workbook.close();
		String JSONMap = JsonParser.getJSON(dataByCountryOrRegion);
		PdfParser.writeText(JSONMap);
		return JSONMap;
	}

	public String getDataByYear() throws IOException {
		Sheet sheet = getDatabase();
		Map<String, Long> dataByYear = new LinkedHashMap<>();

		for (Cell cell : sheet.getRow(5)) {
			String cellValue = dataFormatter.formatCellValue(cell).replaceAll("\\D", "");
			if (cellValue.equals(this.getYear())) {
				int cellIndex = cell.getColumnIndex();
				while (rowIndex < 166) {
					for (Cell c : sheet.getRow(rowIndex)) {
						if (c.getCellTypeEnum().name().equals("STRING")) {
							String CountryOrRegionName = dataFormatter.formatCellValue(c);
							vehiclesSaled = Long.parseLong(dataFormatter
									.formatCellValue(workbook.getSheetAt(0).getRow(rowIndex).getCell(cellIndex))
									.replaceAll("\\D", ""));
							dataByYear.put(CountryOrRegionName, vehiclesSaled);
						}
					}
					rowIndex++;
				}
			}
		}
		workbook.close();
		String JSONMap = JsonParser.getJSON(dataByYear);
		PdfParser.writeText(JSONMap);
		return JsonParser.getJSON(dataByYear);
	}

	public String getDataByVehiclesSoldRange() throws IOException {
		Long startR = Long.parseLong(this.getStartRange());
		Long endR = Long.parseLong(this.getEndRange());
		Sheet sheet = getDatabase();
		Map<String, Map<Integer, Long>> dataByVehiclesSoldRange = new LinkedHashMap<>();
		int checkIfHasParticularSoldRange = 0;
		int year = 0;
		String countryOrRegionName = "";

		while (rowIndex < 166) {
			Map<Integer, Long> dataByYear = new LinkedHashMap<>();
			checkIfHasParticularSoldRange = 0;
			for (Cell cell : sheet.getRow(rowIndex)) {
				if (cell.getCellTypeEnum().name().equals("NUMERIC")) {
					long cellValue = Long.parseLong(dataFormatter.formatCellValue(cell).replaceAll("\\D", ""));
					if (cellValue >= startR && cellValue <= endR) {
						checkIfHasParticularSoldRange++;
						year = Integer.parseInt(dataFormatter
								.formatCellValue(workbook.getSheetAt(0).getRow(5).getCell(cell.getColumnIndex())));
						countryOrRegionName = dataFormatter
								.formatCellValue(workbook.getSheetAt(0).getRow(rowIndex).getCell(0));
						dataByYear.put(year, cellValue);
						dataByVehiclesSoldRange.put(countryOrRegionName, dataByYear);
					}
				}
				if (checkIfHasParticularSoldRange == 0) {
					countryOrRegionName = dataFormatter
							.formatCellValue(workbook.getSheetAt(0).getRow(rowIndex).getCell(0));
					dataByVehiclesSoldRange.put(countryOrRegionName, null);
				}
			}
			rowIndex++;
		}
		workbook.close();
		String JSONMap = JsonParser.getJSON(dataByVehiclesSoldRange);
		PdfParser.writeText(JSONMap);
		return JSONMap;
	}

	public String getAllSalesData() throws IOException {
		Sheet sheet = getDatabase();
		Map<String, Map<Integer, Long>> allDataSales = new LinkedHashMap<>();
		int year = 0;
		String countryOrRegionName = "";

		while (rowIndex < 166) {
			Map<Integer, Long> dataByYear = new LinkedHashMap<>();
			for (Cell cell : sheet.getRow(rowIndex)) {
				if (cell.getCellTypeEnum().name().equals("NUMERIC")) {
					long cellValue = Long.parseLong(dataFormatter.formatCellValue(cell).replaceAll("\\D", ""));
					year = Integer.parseInt(dataFormatter
							.formatCellValue(workbook.getSheetAt(0).getRow(5).getCell(cell.getColumnIndex())));
					countryOrRegionName = dataFormatter
							.formatCellValue(workbook.getSheetAt(0).getRow(rowIndex).getCell(0));
					dataByYear.put(year, cellValue);
					allDataSales.put(countryOrRegionName, dataByYear);
				}
			}
			rowIndex++;
		}
		workbook.close();
		String JSONMap = JsonParser.getJSON(allDataSales);
		PdfParser.writeText(JSONMap);
		return JsonParser.getJSON(allDataSales);
	}

	private Sheet getDatabase() {
		workbook = DBConnector.openVehiclesDataFile();
		Sheet sheet = workbook.getSheetAt(0);
		return sheet;
	}

	
	
	

	


}
