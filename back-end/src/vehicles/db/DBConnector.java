package vehicles.db;


import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class DBConnector {

	public static final String VEHICLES_DATA_FILE = "Sales-Passenger-cars-2017.xlsx";
	
	public static Workbook openVehiclesDataFile(){
		try {
		Workbook workbook = WorkbookFactory.create(new File(VEHICLES_DATA_FILE));
		return workbook;
		}catch(InvalidFormatException | IOException ex) {}
		return null;
	}

}
