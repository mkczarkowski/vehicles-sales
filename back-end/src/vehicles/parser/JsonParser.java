package vehicles.parser;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonParser {

	public static String getJSON(Map map) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String jsonString = gson.toJson(map);
		return jsonString;
	}
	
}
